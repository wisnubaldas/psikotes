<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class MenuBar extends Component
{
    public function user()
    {
        return [
            [
                'name' => 'Dashboard',
                'icon' => 'home',
                'link' => '/',
                'children'=>null,
            ],
            [
                'name' => 'User',
                'icon' => 'file-text',
                'link' => '#',
                'children'=>[
                    [
                        'name' => 'User1',
                        'icon' => 'home',
                        'link' => '/child1',
                    ],
                    [
                        'name' => 'User2',
                        'icon' => 'home',
                        'link' => '/child2',
                    ],
                ],
            ],
        ];
    }
    public function admin()
    {
        return [
            [
                'name' => 'Dashboard',
                'icon' => 'home',
                'link' => '/',
                'children'=>null,
            ],
            [
                'name' => 'menu1',
                'icon' => 'file-text',
                'link' => '#',
                'children'=>[
                    [
                        'name' => 'child1',
                        'icon' => 'home',
                        'link' => '/child1',
                    ],
                    [
                        'name' => 'child2',
                        'icon' => 'home',
                        'link' => '/child2',
                    ],
                ],
            ],
        ];
    }
    public function render()
    {
        if(Auth::user()->hasRole('admin')){
            $menu = $this->admin();
        }else{
            $menu = $this->user();
        }
        return view('livewire.menu-bar',['menu' => $menu]);
    }
}
