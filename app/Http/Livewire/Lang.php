<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Lang extends Component
{
    public function render()
    {
        return view('livewire.lang');
    }
}
