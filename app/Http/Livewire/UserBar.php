<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UserBar extends Component
{
    public function render()
    {
        return view('livewire.user-bar');
    }
}
