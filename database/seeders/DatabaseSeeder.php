<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Administrator',
            'email' => 'wisnubaldas@gmail.com',
        ]);
        \App\Models\User::factory()->create([
            'name' => 'Candidate',
            'email' => 'candidate@gmail.com',
        ]);
        $r_admin = Role::create(['name' => 'admin']);
        $p_admin = Permission::create(['name' => 'delete']);
        $r_admin->givePermissionTo($p_admin);
        $p_admin->assignRole($r_admin);

        $r_candidate = Role::create(['name' => 'candidate']);
        $p_candidate = Permission::create(['name' => 'view']);
        $r_candidate->givePermissionTo($p_candidate);
        $p_candidate->assignRole($r_candidate);

        $user =  \App\Models\User::find(1);
        $user->givePermissionTo('delete');
        $user->assignRole('admin');

        $user =  \App\Models\User::find(2);
        $user->givePermissionTo('view');
        $user->assignRole('candidate');

    }
}
