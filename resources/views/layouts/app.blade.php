<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description"/>
    <meta content="Themesbrand" name="author"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    @include('layouts.css')
    @livewireStyles
</head>
<body data-layout-size="boxed" data-layout="horizontal">
    <!-- Begin page -->
    <div id="layout-wrapper">
    @includeWhen($menuLayout, 'layouts.menu')
    {{-- @include('layouts.menu') --}}
    @yield('content')
    </div>
    @include('layouts.js')
    <!-- apexcharts -->
    <script src="assets/libs/apexcharts/apexcharts.min.js"></script>

    <!-- Plugins js-->
    <script src="/assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/assets/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
    <!-- dashboard init -->
    {{-- <script src="assets/js/pages/dashboard.init.js"></script> --}}

    <script src="/assets/js/app.js"></script>
    
    @livewireScripts
</body>
</html>
