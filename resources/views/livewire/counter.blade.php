<div>
    {{-- Because she competes with no one, no one can compete with her. --}}
    <h1>Hello World!</h1>
    <div style="text-align: center">

        <button wire:click="increment"><h1>+</h1></button>
    
        <h1>{{ $count }}</h1>
    
    </div>
</div>
