<div>
    <div class="dropdown d-none d-sm-inline-block">
        <button type="button" class="btn header-item" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="me-2" src="/assets/images/flags/us.jpg" alt="Header Language" height="16">
        </button>
        <div class="dropdown-menu dropdown-menu-end">

           <!-- item-->
           <a href="?lang=en" class="dropdown-item notify-item">
                <img src="/assets/images/flags/us.jpg" alt="user-image" class="me-1" height="12"> <span class="align-middle"> English </span>
            </a>
            
            <!-- item-->
            <a href="?lang=de" class="dropdown-item notify-item">
                <img src="/assets/images/flags/germany.jpg" alt="user-image" class="me-1" height="12"> <span class="align-middle"> German </span>
            </a>

            <!-- item-->
            <a href="?lang=it" class="dropdown-item notify-item">
                <img src="/assets/images/flags/italy.jpg" alt="user-image" class="me-1" height="12"> <span class="align-middle"> Italian </span>
            </a>

            <!-- item-->
            <a href="?lang=es" class="dropdown-item notify-item">
                <img src="/assets/images/flags/spain.jpg" alt="user-image" class="me-1" height="12"> <span class="align-middle"> Spanish </span>
            </a>

             <!-- item-->
             <a href="?lang=ru" class="dropdown-item notify-item">
                <img src="/assets/images/flags/russia.jpg" alt="user-image" class="me-1" height="12"> <span class="align-middle"> Russian </span>
            </a>

        </div>
    </div>
</div>
