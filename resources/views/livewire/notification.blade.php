<div>
    {{-- The whole world belongs to you. --}}
    <div class="dropdown d-inline-block">
        <button type="button" class="btn header-item noti-icon position-relative" id="page-header-notifications-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i data-feather="bell" class="icon-lg"></i>
            <span class="badge bg-danger rounded-pill">5</span>
        </button>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-notifications-dropdown">
            <div class="p-3">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="m-0"> Notifications </h6>
                    </div>
                    <div class="col-auto">
                        <a href="#!" class="small text-reset text-decoration-underline"> Unread (3)</a>
                    </div>
                </div>
            </div>
            <div data-simplebar style="max-height: 230px;">
                <a href="#!" class="text-reset notification-item">
                    <div class="d-flex">
                        <img src="assets/images/users/avatar-3.jpg" class="me-3 rounded-circle avatar-sm" alt="user-pic">
                        <div class="flex-grow-1">
                            <h6 class="mb-1">James_Lemire</h6>
                            <div class="font-size-13 text-muted">
                                <p class="mb-1">It_will_seem_like_simplified_English.</p>
                                <p class="mb-0"><i class="mdi mdi-clock-outline"></i> <span>1_hours_ago</span></p>
                            </div>
                        </div>
                    </div>
                </a>
                
            </div>
            <div class="p-2 border-top d-grid">
                <a class="btn btn-sm btn-link font-size-14 text-center" href="javascript:void(0)">
                    <i class="mdi mdi-arrow-right-circle me-1"></i> <span>View_More</span>
                </a>
            </div>
        </div>
    </div>
</div>
